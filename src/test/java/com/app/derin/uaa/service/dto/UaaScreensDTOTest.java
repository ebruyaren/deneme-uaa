package com.app.derin.uaa.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaScreensDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaScreensDTO.class);
        UaaScreensDTO uaaScreensDTO1 = new UaaScreensDTO();
        uaaScreensDTO1.setId(1L);
        UaaScreensDTO uaaScreensDTO2 = new UaaScreensDTO();
        assertThat(uaaScreensDTO1).isNotEqualTo(uaaScreensDTO2);
        uaaScreensDTO2.setId(uaaScreensDTO1.getId());
        assertThat(uaaScreensDTO1).isEqualTo(uaaScreensDTO2);
        uaaScreensDTO2.setId(2L);
        assertThat(uaaScreensDTO1).isNotEqualTo(uaaScreensDTO2);
        uaaScreensDTO1.setId(null);
        assertThat(uaaScreensDTO1).isNotEqualTo(uaaScreensDTO2);
    }
}
