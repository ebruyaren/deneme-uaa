package com.app.derin.uaa.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.app.derin.uaa.web.rest.TestUtil;

public class UaaRolesDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UaaRolesDTO.class);
        UaaRolesDTO uaaRolesDTO1 = new UaaRolesDTO();
        uaaRolesDTO1.setId(1L);
        UaaRolesDTO uaaRolesDTO2 = new UaaRolesDTO();
        assertThat(uaaRolesDTO1).isNotEqualTo(uaaRolesDTO2);
        uaaRolesDTO2.setId(uaaRolesDTO1.getId());
        assertThat(uaaRolesDTO1).isEqualTo(uaaRolesDTO2);
        uaaRolesDTO2.setId(2L);
        assertThat(uaaRolesDTO1).isNotEqualTo(uaaRolesDTO2);
        uaaRolesDTO1.setId(null);
        assertThat(uaaRolesDTO1).isNotEqualTo(uaaRolesDTO2);
    }
}
