package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.AuthServiceApp;
import com.app.derin.uaa.config.SecurityBeanOverrideConfiguration;
import com.app.derin.uaa.domain.UaaUsers;
import com.app.derin.uaa.repository.UaaUsersRepository;
import com.app.derin.uaa.service.UaaUsersService;
import com.app.derin.uaa.service.dto.UaaUsersDTO;
import com.app.derin.uaa.service.mapper.UaaUsersMapper;
import com.app.derin.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.derin.uaa.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UaaUsersResource} REST controller.
 */
@SpringBootTest(classes = AuthServiceApp.class)
public class UaaUsersResourceIT {

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_USER_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_USER_MAIL = "BBBBBBBBBB";

    @Autowired
    private UaaUsersRepository uaaUsersRepository;

    @Autowired
    private UaaUsersMapper uaaUsersMapper;

    @Autowired
    private UaaUsersService uaaUsersService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUaaUsersMockMvc;

    private UaaUsers uaaUsers;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UaaUsersResource uaaUsersResource = new UaaUsersResource(uaaUsersService);
        this.restUaaUsersMockMvc = MockMvcBuilders.standaloneSetup(uaaUsersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaUsers createEntity(EntityManager em) {
        UaaUsers uaaUsers = new UaaUsers()
            .userName(DEFAULT_USER_NAME)
            .userMail(DEFAULT_USER_MAIL);
        return uaaUsers;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UaaUsers createUpdatedEntity(EntityManager em) {
        UaaUsers uaaUsers = new UaaUsers()
            .userName(UPDATED_USER_NAME)
            .userMail(UPDATED_USER_MAIL);
        return uaaUsers;
    }

    @BeforeEach
    public void initTest() {
        uaaUsers = createEntity(em);
    }

    @Test
    @Transactional
    public void createUaaUsers() throws Exception {
        int databaseSizeBeforeCreate = uaaUsersRepository.findAll().size();

        // Create the UaaUsers
        UaaUsersDTO uaaUsersDTO = uaaUsersMapper.toDto(uaaUsers);
        restUaaUsersMockMvc.perform(post("/api/uaa-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaUsersDTO)))
            .andExpect(status().isCreated());

        // Validate the UaaUsers in the database
        List<UaaUsers> uaaUsersList = uaaUsersRepository.findAll();
        assertThat(uaaUsersList).hasSize(databaseSizeBeforeCreate + 1);
        UaaUsers testUaaUsers = uaaUsersList.get(uaaUsersList.size() - 1);
        assertThat(testUaaUsers.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testUaaUsers.getUserMail()).isEqualTo(DEFAULT_USER_MAIL);
    }

    @Test
    @Transactional
    public void createUaaUsersWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uaaUsersRepository.findAll().size();

        // Create the UaaUsers with an existing ID
        uaaUsers.setId(1L);
        UaaUsersDTO uaaUsersDTO = uaaUsersMapper.toDto(uaaUsers);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUaaUsersMockMvc.perform(post("/api/uaa-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaUsersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaUsers in the database
        List<UaaUsers> uaaUsersList = uaaUsersRepository.findAll();
        assertThat(uaaUsersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUaaUsers() throws Exception {
        // Initialize the database
        uaaUsersRepository.saveAndFlush(uaaUsers);

        // Get all the uaaUsersList
        restUaaUsersMockMvc.perform(get("/api/uaa-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uaaUsers.getId().intValue())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
            .andExpect(jsonPath("$.[*].userMail").value(hasItem(DEFAULT_USER_MAIL)));
    }
    
    @Test
    @Transactional
    public void getUaaUsers() throws Exception {
        // Initialize the database
        uaaUsersRepository.saveAndFlush(uaaUsers);

        // Get the uaaUsers
        restUaaUsersMockMvc.perform(get("/api/uaa-users/{id}", uaaUsers.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(uaaUsers.getId().intValue()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
            .andExpect(jsonPath("$.userMail").value(DEFAULT_USER_MAIL));
    }

    @Test
    @Transactional
    public void getNonExistingUaaUsers() throws Exception {
        // Get the uaaUsers
        restUaaUsersMockMvc.perform(get("/api/uaa-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUaaUsers() throws Exception {
        // Initialize the database
        uaaUsersRepository.saveAndFlush(uaaUsers);

        int databaseSizeBeforeUpdate = uaaUsersRepository.findAll().size();

        // Update the uaaUsers
        UaaUsers updatedUaaUsers = uaaUsersRepository.findById(uaaUsers.getId()).get();
        // Disconnect from session so that the updates on updatedUaaUsers are not directly saved in db
        em.detach(updatedUaaUsers);
        updatedUaaUsers
            .userName(UPDATED_USER_NAME)
            .userMail(UPDATED_USER_MAIL);
        UaaUsersDTO uaaUsersDTO = uaaUsersMapper.toDto(updatedUaaUsers);

        restUaaUsersMockMvc.perform(put("/api/uaa-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaUsersDTO)))
            .andExpect(status().isOk());

        // Validate the UaaUsers in the database
        List<UaaUsers> uaaUsersList = uaaUsersRepository.findAll();
        assertThat(uaaUsersList).hasSize(databaseSizeBeforeUpdate);
        UaaUsers testUaaUsers = uaaUsersList.get(uaaUsersList.size() - 1);
        assertThat(testUaaUsers.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testUaaUsers.getUserMail()).isEqualTo(UPDATED_USER_MAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingUaaUsers() throws Exception {
        int databaseSizeBeforeUpdate = uaaUsersRepository.findAll().size();

        // Create the UaaUsers
        UaaUsersDTO uaaUsersDTO = uaaUsersMapper.toDto(uaaUsers);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUaaUsersMockMvc.perform(put("/api/uaa-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(uaaUsersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UaaUsers in the database
        List<UaaUsers> uaaUsersList = uaaUsersRepository.findAll();
        assertThat(uaaUsersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUaaUsers() throws Exception {
        // Initialize the database
        uaaUsersRepository.saveAndFlush(uaaUsers);

        int databaseSizeBeforeDelete = uaaUsersRepository.findAll().size();

        // Delete the uaaUsers
        restUaaUsersMockMvc.perform(delete("/api/uaa-users/{id}", uaaUsers.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UaaUsers> uaaUsersList = uaaUsersRepository.findAll();
        assertThat(uaaUsersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
