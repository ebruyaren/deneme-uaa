package com.app.derin.uaa.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A UaaUsers.
 */
@Entity
@Table(name = "uaa_users")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UaaUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_mail")
    private String userMail;

    @ManyToMany(mappedBy = "users")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<UaaRoles> roles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public UaaUsers userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMail() {
        return userMail;
    }

    public UaaUsers userMail(String userMail) {
        this.userMail = userMail;
        return this;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public Set<UaaRoles> getRoles() {
        return roles;
    }

    public UaaUsers roles(Set<UaaRoles> uaaRoles) {
        this.roles = uaaRoles;
        return this;
    }

    public UaaUsers addRoles(UaaRoles uaaRoles) {
        this.roles.add(uaaRoles);
        uaaRoles.getUsers().add(this);
        return this;
    }

    public UaaUsers removeRoles(UaaRoles uaaRoles) {
        this.roles.remove(uaaRoles);
        uaaRoles.getUsers().remove(this);
        return this;
    }

    public void setRoles(Set<UaaRoles> uaaRoles) {
        this.roles = uaaRoles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UaaUsers)) {
            return false;
        }
        return id != null && id.equals(((UaaUsers) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UaaUsers{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", userMail='" + getUserMail() + "'" +
            "}";
    }
}
