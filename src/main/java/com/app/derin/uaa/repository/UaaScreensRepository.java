package com.app.derin.uaa.repository;

import com.app.derin.uaa.domain.UaaScreens;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UaaScreens entity.
 */
@Repository
public interface UaaScreensRepository extends JpaRepository<UaaScreens, Long> {

    @Query(value = "select distinct uaaScreens from UaaScreens uaaScreens left join fetch uaaScreens.roles",
        countQuery = "select count(distinct uaaScreens) from UaaScreens uaaScreens")
    Page<UaaScreens> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct uaaScreens from UaaScreens uaaScreens left join fetch uaaScreens.roles")
    List<UaaScreens> findAllWithEagerRelationships();

    @Query("select uaaScreens from UaaScreens uaaScreens left join fetch uaaScreens.roles where uaaScreens.id =:id")
    Optional<UaaScreens> findOneWithEagerRelationships(@Param("id") Long id);

}
