package com.app.derin.uaa.repository;

import com.app.derin.uaa.domain.UaaRoles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UaaRoles entity.
 */
@Repository
public interface UaaRolesRepository extends JpaRepository<UaaRoles, Long> {

    @Query(value = "select distinct uaaRoles from UaaRoles uaaRoles left join fetch uaaRoles.users",
        countQuery = "select count(distinct uaaRoles) from UaaRoles uaaRoles")
    Page<UaaRoles> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct uaaRoles from UaaRoles uaaRoles left join fetch uaaRoles.users")
    List<UaaRoles> findAllWithEagerRelationships();

    @Query("select uaaRoles from UaaRoles uaaRoles left join fetch uaaRoles.users where uaaRoles.id =:id")
    Optional<UaaRoles> findOneWithEagerRelationships(@Param("id") Long id);

}
