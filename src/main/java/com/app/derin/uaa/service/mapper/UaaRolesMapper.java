package com.app.derin.uaa.service.mapper;

import com.app.derin.uaa.domain.*;
import com.app.derin.uaa.service.dto.UaaRolesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UaaRoles} and its DTO {@link UaaRolesDTO}.
 */
@Mapper(componentModel = "spring", uses = {UaaUsersMapper.class})
public interface UaaRolesMapper extends EntityMapper<UaaRolesDTO, UaaRoles> {


    @Mapping(target = "removeUsers", ignore = true)
    @Mapping(target = "screens", ignore = true)
    @Mapping(target = "removeScreens", ignore = true)
    UaaRoles toEntity(UaaRolesDTO uaaRolesDTO);

    default UaaRoles fromId(Long id) {
        if (id == null) {
            return null;
        }
        UaaRoles uaaRoles = new UaaRoles();
        uaaRoles.setId(id);
        return uaaRoles;
    }
}
