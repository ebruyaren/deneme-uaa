package com.app.derin.uaa.service;

import com.app.derin.uaa.service.dto.UaaUsersDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.app.derin.uaa.domain.UaaUsers}.
 */
public interface UaaUsersService {

    /**
     * Save a uaaUsers.
     *
     * @param uaaUsersDTO the entity to save.
     * @return the persisted entity.
     */
    UaaUsersDTO save(UaaUsersDTO uaaUsersDTO);

    /**
     * Get all the uaaUsers.
     *
     * @return the list of entities.
     */
    List<UaaUsersDTO> findAll();


    /**
     * Get the "id" uaaUsers.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UaaUsersDTO> findOne(Long id);

    /**
     * Delete the "id" uaaUsers.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
