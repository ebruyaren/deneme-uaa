package com.app.derin.uaa.ext.roles;

import com.app.derin.uaa.service.dto.UaaRolesDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UaaRolesBusService {

    UaaRolesDTO save(UaaRolesDTO uaaRolesDTO);

    Page<UaaRolesDTO> findAll(Pageable pageable);

    UaaRolesDTO findOne(Long id);

    void delete(Long id);

}
