package com.app.derin.uaa.ext.users;

import com.app.derin.uaa.domain.UaaUsers;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UaaUsers entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UaaUsersBusRepository extends JpaRepository<UaaUsers, Long> {

}
