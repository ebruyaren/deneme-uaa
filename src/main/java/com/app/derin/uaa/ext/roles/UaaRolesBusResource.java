package com.app.derin.uaa.ext.roles;

import com.app.derin.uaa.service.dto.UaaRolesDTO;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;

import io.github.jhipster.web.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")

public class UaaRolesBusResource{
    private final Logger log = LoggerFactory.getLogger(com.app.derin.uaa.ext.roles.UaaRolesBusResource.class);
    private static final String ENTITY_NAME = "role";
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaRolesBusService uaaRolesBusService;


    public UaaRolesBusResource(UaaRolesBusService uaaRolesBusService) {
        this.uaaRolesBusService = uaaRolesBusService;
    }

    @PostMapping("/ext/roles")

    public ResponseEntity<UaaRolesDTO> createRole(@RequestBody UaaRolesDTO uaaRolesDTO) throws URISyntaxException {
        log.debug("REST request to save Role : {}", uaaRolesDTO);
        if (uaaRolesDTO.getId() != null) {
            throw new BadRequestAlertException("A new course cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaRolesDTO result = uaaRolesBusService.save(uaaRolesDTO);
        return ResponseEntity.created(new URI("/api/roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    @PutMapping("/ext/roles")
    public ResponseEntity<UaaRolesDTO> updateRole(@RequestBody UaaRolesDTO uaaRolesDTO) throws URISyntaxException {
        log.debug("REST request to update Role : {}", uaaRolesDTO);
        if (uaaRolesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaRolesDTO result = uaaRolesBusService.save(uaaRolesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaRolesDTO.getId().toString()))
            .body(result);
    }

    @GetMapping("/ext/roles/all")
    public ResponseEntity<List<UaaRolesDTO>> getAllUaaRoles(Pageable pageable) {
        log.debug("REST request to get a page of Roles");
        Page<UaaRolesDTO> page = uaaRolesBusService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
//        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @DeleteMapping("/ext/roles/{id}")
    public ResponseEntity<Void> deleteRoles(@PathVariable Long id) {
        log.debug("REST request to delete Course : {}", id);
        uaaRolesBusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


}
