package com.app.derin.uaa.ext.roles;


import com.app.derin.uaa.domain.UaaRoles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class UaaRolesBusRepositoryImpl implements UaaRolesBusRepository {

    @PersistenceContext
    private EntityManager entityManager;
    private List<String> roles;
    private Object Page;

    @Override
    public UaaRoles getOne(long id){
        String strQuery = "SELECT DISTINCT * FROM uaa_roles roles WHERE users.id =:entity_id";
        Query query = entityManager.createNativeQuery(strQuery, UaaRoles.class);
        ((Query) query).setParameter("entity_id", id);
        return (UaaRoles) query.getResultStream().findFirst().orElse(null);
    }

    @Override
    public Page<UaaRoles> getAll(Pageable pageable) {
        String strQuery = "SELECT DISTINCT * FROM uaa_roles roles";
        Query query = entityManager.createNativeQuery(strQuery, UaaRoles.class);
        List<UaaRoles> rolesList = query.getResultList();
        Page<UaaRoles> page = new PageImpl<>(rolesList);
        Page p1 = new PageImpl<>(rolesList, pageable, rolesList.size());
        return page;
    }

    @Override
    public UaaRoles save(UaaRoles uaaRoles) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }


}
