package com.app.derin.uaa.web.rest;

import com.app.derin.uaa.service.UaaUsersService;
import com.app.derin.uaa.web.rest.errors.BadRequestAlertException;
import com.app.derin.uaa.service.dto.UaaUsersDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.app.derin.uaa.domain.UaaUsers}.
 */
@RestController
@RequestMapping("/api")
public class UaaUsersResource {

    private final Logger log = LoggerFactory.getLogger(UaaUsersResource.class);

    private static final String ENTITY_NAME = "uaaUsers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UaaUsersService uaaUsersService;

    public UaaUsersResource(UaaUsersService uaaUsersService) {
        this.uaaUsersService = uaaUsersService;
    }

    /**
     * {@code POST  /uaa-users} : Create a new uaaUsers.
     *
     * @param uaaUsersDTO the uaaUsersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uaaUsersDTO, or with status {@code 400 (Bad Request)} if the uaaUsers has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/uaa-users")
    public ResponseEntity<UaaUsersDTO> createUaaUsers(@RequestBody UaaUsersDTO uaaUsersDTO) throws URISyntaxException {
        log.debug("REST request to save UaaUsers : {}", uaaUsersDTO);
        if (uaaUsersDTO.getId() != null) {
            throw new BadRequestAlertException("A new uaaUsers cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UaaUsersDTO result = uaaUsersService.save(uaaUsersDTO);
        return ResponseEntity.created(new URI("/api/uaa-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /uaa-users} : Updates an existing uaaUsers.
     *
     * @param uaaUsersDTO the uaaUsersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uaaUsersDTO,
     * or with status {@code 400 (Bad Request)} if the uaaUsersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uaaUsersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/uaa-users")
    public ResponseEntity<UaaUsersDTO> updateUaaUsers(@RequestBody UaaUsersDTO uaaUsersDTO) throws URISyntaxException {
        log.debug("REST request to update UaaUsers : {}", uaaUsersDTO);
        if (uaaUsersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UaaUsersDTO result = uaaUsersService.save(uaaUsersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, uaaUsersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /uaa-users} : get all the uaaUsers.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uaaUsers in body.
     */
    @GetMapping("/uaa-users")
    public List<UaaUsersDTO> getAllUaaUsers() {
        log.debug("REST request to get all UaaUsers");
        return uaaUsersService.findAll();
    }

    /**
     * {@code GET  /uaa-users/:id} : get the "id" uaaUsers.
     *
     * @param id the id of the uaaUsersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uaaUsersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/uaa-users/{id}")
    public ResponseEntity<UaaUsersDTO> getUaaUsers(@PathVariable Long id) {
        log.debug("REST request to get UaaUsers : {}", id);
        Optional<UaaUsersDTO> uaaUsersDTO = uaaUsersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uaaUsersDTO);
    }

    /**
     * {@code DELETE  /uaa-users/:id} : delete the "id" uaaUsers.
     *
     * @param id the id of the uaaUsersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/uaa-users/{id}")
    public ResponseEntity<Void> deleteUaaUsers(@PathVariable Long id) {
        log.debug("REST request to delete UaaUsers : {}", id);
        uaaUsersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
